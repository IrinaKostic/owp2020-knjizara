DROP SCHEMA IF EXISTS knjizara;
CREATE SCHEMA knjizara DEFAULT CHARACTER SET utf8;
USE knjizara;

CREATE TABLE korisnici (
	korisnickoIme VARCHAR(20),
	lozinka VARCHAR(20) NOT NULL,
	email VARCHAR(20) NOT NULL,
    ime VARCHAR(20) NOT NULL,
    prezime VARCHAR(20) NOT NULL,
    datumRodjenja DATE,
    adresa VARCHAR(20) NOT NULL,
    brTelefona INT NOT NULL,
    datumVremeReg DATETIME,
	uloga ENUM('admin', 'korisnik') DEFAULT 'korisnik',
	administrator BOOL DEFAULT false,
	PRIMARY KEY(korisnickoIme)
);




CREATE TABLE knjige (
	id BIGINT AUTO_INCREMENT,
	naziv VARCHAR(30) NOT NULL,
	ISBN INT NOT NULL,
	autor VARCHAR(30) NOT NULL,
    godIzdavanja DATE,
    opis VARCHAR(30) NOT NULL,
    cena DECIMAL(10, 2) NOT NULL,
    brStranica INT NOT NULL,
    tipPoveza ENUM('tvrdi', 'meki') DEFAULT 'tvrdi',
	pismo ENUM('latinica', 'cirilica') DEFAULT 'cirilica',
	jezik VARCHAR(30) NOT NULL,
    prosecnaOcena INT NOT NULL, 
	PRIMARY KEY(id)
);


CREATE TABLE zanrovi (
	id BIGINT AUTO_INCREMENT,
	ime VARCHAR(25) NOT NULL,
	opis VARCHAR(30) NOT NULL,
	PRIMARY KEY(id)
);

INSERT INTO korisnici (korisnickoIme, lozinka, email,ime, prezime,datumRodjenja, adresa, brTelefona,datumVremeReg,uloga,administrator)
VALUES ('aki', 'al', 'a@a.com','ai','ap','2020-06-22','adresa',1111,'2020-06-22 20:00', 'admin', true);
INSERT INTO korisnici (korisnickoIme, lozinka, email,ime, prezime,datumRodjenja, adresa, brTelefona,datumVremeReg,uloga,administrator)
VALUES ('bki', 'bl', 'b@b.com','bi','bp','2020-06-22','adresa',2222,'2020-06-22 21:00', 'korisnik', false);
INSERT INTO korisnici (korisnickoIme, lozinka, email,ime, prezime,datumRodjenja, adresa, brTelefona,datumVremeReg,uloga,administrator) 
VALUES ('cki', 'cl', 'c@c.com','ci','cp','2020-06-22','adresa',3333,'2020-06-22 22:00', 'admin', true);

INSERT INTO knjige (id, naziv, ISBN, autor, godIzdavanja, opis, cena, brStranica, tipPoveza, pismo, jezik, prosecnaOcena) VALUES (1, 'Jasan kod', 111111 ,'Robert C. Martin', '2003-05-24','opis','2000.00', 1000, 'tvrdi', 'latinica', 'srpski', 10);
INSERT INTO knjige (id, naziv, ISBN, autor, godIzdavanja, opis, cena, brStranica, tipPoveza, pismo, jezik, prosecnaOcena) VALUES (2, 'Moc podsvesti', 22222 ,'Džozef Marfi', '2003-05-24','opis','1000.00', 200, 'meki', 'latinica', 'srpski', 10);
INSERT INTO knjige (id, naziv, ISBN, autor, godIzdavanja, opis, cena, brStranica, tipPoveza, pismo, jezik, prosecnaOcena) VALUES (3, 'Put kojim se redje ide', 33333zanrovikorisniciknjige ,'Skot Pek', '2003-06-24','opis','700.00', 150, 'tvrdi', 'latinica', 'srpski', 10);
 

INSERT INTO zanrovi (id, ime, opis) VALUES (1, 'naučna fantastika', 'zanropis');
INSERT INTO zanrovi (id, ime, opis) VALUES (2, 'horor', 'zanropis');
INSERT INTO zanrovi (id, ime, opis) VALUES (3, 'komedija', 'zanropis');





