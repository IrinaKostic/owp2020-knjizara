package com.sf612017.KnjizaraIrina;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;



@SpringBootApplication
public class KnjizaraIrinaApplication extends SpringBootServletInitializer {
	
	@Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(KnjizaraIrinaApplication.class);
    }

	public static void main(String[] args) {
		SpringApplication.run(KnjizaraIrinaApplication.class, args);
	}

}
