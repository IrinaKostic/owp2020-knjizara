package com.sf612017.KnjizaraIrina.Model;

import java.time.LocalDate;




public class Knjiga {
	
	Long id;
	String naziv;
	int ISBN;
	String izdavackaKuca;
	String autor;
	LocalDate godinaIzdavanja;
	String opis;
	//slika
	Double cena;
	int brStranica;
	String tipPoveza;//enum tvrdi ili meki 
	String pismo;//enum latinica ili cirilica
	String jezik;
	int ProsecnaOcena;
	
	
	

	public Knjiga(Long id, String naziv, int iSBN, String izdavackaKuca, String autor, LocalDate godinaIzdavanja,
			String opis, Double cena, int brStranica, String tipPoveza, String pismo, String jezik, int prosecnaOcena) {
		super();
		this.id = id;
		this.naziv = naziv;
		ISBN = iSBN;
		this.izdavackaKuca = izdavackaKuca;
		this.autor = autor;
		this.godinaIzdavanja = godinaIzdavanja;
		this.opis = opis;
		this.cena = cena;
		this.brStranica = brStranica;
		this.tipPoveza = tipPoveza;
		this.pismo = pismo;
		this.jezik = jezik;
		ProsecnaOcena = prosecnaOcena;
	}

	
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime*result + ((id == null) ? 0 : id.hashCode());
		return 31 + id.hashCode();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Knjiga other = (Knjiga) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}



	public Long getId() {
		return id;
	}




	public void setId(Long id) {
		this.id = id;
	}




	public String getNaziv() {
		return naziv;
	}




	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}




	public int getISBN() {
		return ISBN;
	}




	public void setISBN(int iSBN) {
		ISBN = iSBN;
	}




	public String getIzdavackaKuca() {
		return izdavackaKuca;
	}




	public void setIzdavackaKuca(String izdavackaKuca) {
		this.izdavackaKuca = izdavackaKuca;
	}




	public String getAutor() {
		return autor;
	}




	public void setAutor(String autor) {
		this.autor = autor;
	}




	public LocalDate getGodinaIzdavanja() {
		return godinaIzdavanja;
	}




	public void setGodinaIzdavanja(LocalDate godinaIzdavanja) {
		this.godinaIzdavanja = godinaIzdavanja;
	}




	public String getOpis() {
		return opis;
	}




	public void setOpis(String opis) {
		this.opis = opis;
	}




	public Double getCena() {
		return cena;
	}




	public void setCena(Double cena) {
		this.cena = cena;
	}




	public int getBrStranica() {
		return brStranica;
	}




	public void setBrStranica(int brStranica) {
		this.brStranica = brStranica;
	}




	public String getTipPoveza() {
		return tipPoveza;
	}




	public void setTipPoveza(String tipPoveza) {
		this.tipPoveza = tipPoveza;
	}




	public String getPismo() {
		return pismo;
	}




	public void setPismo(String pismo) {
		this.pismo = pismo;
	}




	public String getJezik() {
		return jezik;
	}




	public void setJezik(String jezik) {
		this.jezik = jezik;
	}




	public int getProsecnaOcena() {
		return ProsecnaOcena;
	}




	public void setProsecnaOcena(int prosecnaOcena) {
		ProsecnaOcena = prosecnaOcena;
	}



	@Override
	public String toString() {
		return "Knjiga [id=" + id + ", naziv=" + naziv + ", ISBN=" + ISBN + ", izdavackaKuca=" + izdavackaKuca
				+ ", autor=" + autor + ", godinaIzdavanja=" + godinaIzdavanja + ", opis=" + opis + ", cena=" + cena
				+ ", brStranica=" + brStranica + ", tipPoveza=" + tipPoveza + ", pismo=" + pismo + ", jezik=" + jezik
				+ ", ProsecnaOcena=" + ProsecnaOcena + "]";
	}


	
	
	
}
