package com.sf612017.KnjizaraIrina.Model;

import java.time.LocalDate;
import java.time.LocalDateTime;




public class Korisnik {

	String korisnickoIme;
	String lozinka;
	String email;
	String ime;
	String prezime;
	LocalDate datumRodjenja;
	String adresa;
	int brTelefona;
	LocalDateTime datumVremeReg;
	String uloga;//enum kupac ili admin
	private boolean administrator = false;
	private boolean ulogovan = false;
	
	
	

	public Korisnik(String korisnickoIme, String lozinka, String email, String ime, String prezime,
			LocalDate datumRodjenja, String adresa, int brTelefona, LocalDateTime datumVremeReg,String uloga, boolean administrator) {
	
		this.korisnickoIme = korisnickoIme;
		this.lozinka = lozinka;
		this.email = email;
		this.ime = ime;
		this.prezime = prezime;
		this.datumRodjenja = datumRodjenja;
		this.adresa = adresa;
		this.brTelefona = brTelefona;
		this.datumVremeReg = datumVremeReg;
		this.uloga= uloga;
		this.administrator = administrator;
		
	}
	
	public Korisnik(String korisnickoIme, String lozinka, String email, String ime, String prezime,
			LocalDate datumRodjenja, String adresa, int brTelefona, LocalDateTime datumVremeReg,String uloga) {
	
		this.korisnickoIme = korisnickoIme;
		this.lozinka = lozinka;
		this.email = email;
		this.ime = ime;
		this.prezime = prezime;
		this.datumRodjenja = datumRodjenja;
		this.adresa = adresa;
		this.brTelefona = brTelefona;
		this.datumVremeReg = datumVremeReg;
		this.uloga= uloga;
	
		
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime*result + ((korisnickoIme == null) ? 0 : korisnickoIme.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Korisnik other = (Korisnik) obj;
		if (korisnickoIme == null) {
			if (other.korisnickoIme != null)
				return false;
		} else if (!korisnickoIme.equals(other.korisnickoIme))
			return false;
		return true;
	}

	public String getKorisnickoIme() {
		return korisnickoIme;
	}

	public void setKorisnickoIme(String korisnickoIme) {
		this.korisnickoIme = korisnickoIme;
	}

	public String getLozinka() {
		return lozinka;
	}

	public void setLozinka(String lozinka) {
		this.lozinka = lozinka;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getIme() {
		return ime;
	}

	public void setIme(String ime) {
		this.ime = ime;
	}

	public String getPrezime() {
		return prezime;
	}

	public void setPrezime(String prezime) {
		this.prezime = prezime;
	}

	public LocalDate getDatumRodjenja() {
		return datumRodjenja;
	}

	public void setDatumRodjenja(LocalDate datumRodjenja) {
		this.datumRodjenja = datumRodjenja;
	}

	public String getAdresa() {
		return adresa;
	}

	public void setAdresa(String adresa) {
		this.adresa = adresa;
	}

	public int getBrTelefona() {
		return brTelefona;
	}

	public void setBrTelefona(int brTelefona) {
		this.brTelefona = brTelefona;
	}

	public LocalDateTime getDatumVremeReg() {
		return datumVremeReg;
	}

	public void setDatumVremeReg(LocalDateTime datumVremeReg) {
		this.datumVremeReg = datumVremeReg;
	}

	
	public String getUloga() {
		return uloga;
	}

	public void setUloga(String uloga) {
		this.uloga = uloga;
	}

	public boolean isAdministrator() {
		return administrator;
	}

	public void setAdministrator(boolean administrator) {
		this.administrator = administrator;
	}

	public boolean isPrijavljen() {
		return ulogovan;
	}

	public void setUlogovan(boolean ulogovan) {
		this.ulogovan = ulogovan;
	}

	@Override
	public String toString() {
		return "Korisnik [korisnickoIme=" + korisnickoIme + ", lozinka=" + lozinka + ", email=" + email + ", ime=" + ime
				+ ", prezime=" + prezime + ", datumRodjenja=" + datumRodjenja + ", adresa=" + adresa + ", brTelefona="
				+ brTelefona + ", datumVremeReg=" + datumVremeReg + ", uloga=" + uloga + ", administrator="
				+ administrator + ", ulogovan=" + ulogovan + "]";
	}

	

	
	
	
	
}
