package com.sf612017.KnjizaraIrina.Model;

public class Zanr {
	
	Long id;
	String ime;
	String opis;
	
	
	
	public Zanr(Long id, String ime, String opis) {
		super();
		this.id = id;
		this.ime = ime;
		this.opis = opis;
	}



	public Long getId() {
		return id;
	}



	public void setId(Long id) {
		this.id = id;
	}



	public String getIme() {
		return ime;
	}



	public void setIme(String ime) {
		this.ime = ime;
	}



	public String getOpis() {
		return opis;
	}



	public void setOpis(String opis) {
		this.opis = opis;
	}
	
	

}
