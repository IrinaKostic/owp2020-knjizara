package com.sf612017.KnjizaraIrina.Model;

import java.util.Date;

public class Komentar {

	enum Status {
		NaCekanju,
		Odobren,
		NijeOdobren
	}
	
	String tekst;
	int oOcena;// od 1 do 5
	Date datumKomentara;
	Korisnik autorKomentara;
	Knjiga knjigaKomentar;
	Status status;

}
