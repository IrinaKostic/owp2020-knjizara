package com.sf612017.KnjizaraIrina.dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

import com.sf612017.KnjizaraIrina.Model.Korisnik;
import com.sf612017.KnjizaraIrina.dao.KorisnikDAO;

public class KorisnikDAOImpl implements KorisnikDAO {

	@Autowired
	private JdbcTemplate jdbcTemplate;

	private class KorisnikRowMapper implements RowMapper<Korisnik> {

		@Override
		public Korisnik mapRow(ResultSet rs, int rowNum) throws SQLException {
			int index = 1;
			String korisnickoIme = rs.getString(index++);
			String lozinka = rs.getString(index++);
			String email = rs.getString(index++);
			String ime = rs.getString(index++);
			String prezime = rs.getString(index++);
			LocalDate datumRodjenja = rs.getDate(index++).toLocalDate();
			String adresa =rs.getString(index++);
			Integer brTelefona = rs.getInt(index++);
			LocalDateTime datumVremeReg = rs.getTimestamp(index++).toLocalDateTime();
			String uloga = rs.getString(index++);
			Boolean administrator = rs.getBoolean(index++);

			Korisnik korisnik = new Korisnik(korisnickoIme, lozinka, email,ime,prezime,datumRodjenja, adresa, brTelefona,datumVremeReg, uloga, administrator);
			return korisnik;
		}

	}
	@Override
	public Korisnik findOne(String korisnickoIme) {
		try {
			String sql = "SELECT korisnickoIme, lozinka, email,ime,prezime,datumRodjenja, adresa, brTelefona,datumVremeReg, uloga, administrator FROM korisnici WHERE korisnickoIme = ?";
			return jdbcTemplate.queryForObject(sql, new KorisnikRowMapper(), korisnickoIme);
		} catch (EmptyResultDataAccessException ex) {
			// ako korisnik nije pronađen
			return null;
		}
	}
	@Override
	public Korisnik findOne(String korisnickoIme, String lozinka) {
		try {
			String sql = "SELECT korisnickoIme, lozinka, email,ime,prezime,datumRodjenja, adresa, brTelefona,datumVremeReg, uloga, administrator FROM korisnici WHERE korisnickoIme = ? AND lozinka = ?";
			return jdbcTemplate.queryForObject(sql, new KorisnikRowMapper(), korisnickoIme, lozinka);
		} catch (EmptyResultDataAccessException ex) {
			// ako korisnik nije pronađen
			return null;
		}
	}
	@Override
	public List<Korisnik> findAll() {
		String sql = "SELECT korisnickoIme, lozinka, email,ime,prezime,datumRodjenja, adresa, brTelefona,datumVremeReg, uloga, administrator FROM korisnici";
		return jdbcTemplate.query(sql, new KorisnikRowMapper());
	}
	@Override
	public List<Korisnik> find(String korisnickoIme,String lozinka,String email,String ime,
			String prezime,LocalDate datumRodjenja,String adresa,int brTelefona,LocalDateTime datumVremeReg,String uloga, Boolean administrator) {
		
		ArrayList<Object> listaArgumenata = new ArrayList<Object>();
		
		String sql = "SELECT korisnickoIme, lozinka, email,ime,prezime,datumRodjenja, adresa, brTelefona,datumVremeReg, uloga, administrator FROM korisnici ";
		
		StringBuffer whereSql = new StringBuffer(" WHERE ");
		boolean imaArgumenata = false;
		
		if(korisnickoIme!=null) {
			korisnickoIme = "%" + korisnickoIme + "%";
			if(imaArgumenata)
				whereSql.append(" AND ");
			whereSql.append("korisnickoIme LIKE ?");
			imaArgumenata = true;
			listaArgumenata.add(korisnickoIme);
		}
		
		if(lozinka!=null) {
			lozinka = "%" + lozinka + "%";
			if(imaArgumenata)
				whereSql.append(" AND ");
			whereSql.append("lozinka LIKE ?");
			imaArgumenata = true;
			listaArgumenata.add(lozinka);
		}
		
		if(email!=null) {
			email = "%" + email + "%";
			if(imaArgumenata)
				whereSql.append(" AND ");
			whereSql.append("email LIKE ?");
			imaArgumenata = true;
			listaArgumenata.add(email);
		}
		
		if(ime!=null) {
			ime = "%" + ime + "%";
			if(imaArgumenata)
				whereSql.append(" AND ");
			whereSql.append("ime LIKE ?");
			imaArgumenata = true;
			listaArgumenata.add(ime);
		} 
		
		if(prezime!=null) {
			prezime = "%" + prezime + "%";
			if(imaArgumenata)
				whereSql.append(" AND ");
			whereSql.append("prezime LIKE ?");
			imaArgumenata = true;
			listaArgumenata.add(prezime);
		}
		
		
		
		if(uloga!=null) {
			uloga = "%" + uloga + "%";
			if(imaArgumenata)
				whereSql.append(" AND ");
			whereSql.append("uloga LIKE ?");
			imaArgumenata = true;
			listaArgumenata.add(uloga);
		}
		
		if(administrator!=null) {	
			//vraća samo administratore ili sve korisnike sistema
			String administratorSql = (administrator)? "administrator = 1": "administrator >= 0";
			if(imaArgumenata)
				whereSql.append(" AND ");
			whereSql.append(administratorSql);
			imaArgumenata = true;
		}
		
		
		if(imaArgumenata)
			sql=sql + whereSql.toString()+" ORDER BY korisnickoIme";
		else
			sql=sql + " ORDER BY korisnickoIme";
		System.out.println(sql);
		
		return jdbcTemplate.query(sql, listaArgumenata.toArray(), new KorisnikRowMapper());
	}
	@Override
	public void save(Korisnik korisnik) {
		String sql = "INSERT INTO korisnici (korisnickoIme, lozinka, email, ime, prezime, datumRodjenja,adresa,brTelefona,datumeVremeRodj,uloga administrator) VALUES (?, ?, ?, ?, ?,?, ?, ?, ?, ?,?)";
		jdbcTemplate.update(sql, korisnik.getKorisnickoIme(), korisnik.getLozinka(), korisnik.getIme(), korisnik.getPrezime(),korisnik.getDatumRodjenja(),korisnik.getAdresa(),korisnik.getBrTelefona(),
				korisnik.getDatumVremeReg(),korisnik.getUloga(),korisnik.isAdministrator());
	}
	@Override
	public void update(Korisnik korisnik) {
		if (korisnik.getLozinka() == null) {
			String sql = "UPDATE korisnici SET email = ?, uloga = ?, administrator = ? WHERE korisnickoIme = ?";
			jdbcTemplate.update(sql, korisnik.getEmail(), korisnik.getUloga(), korisnik.isAdministrator(), korisnik.getKorisnickoIme());
		} else {
			String sql = "UPDATE korisnici SET lozinka = ?, email = ?, uloga = ?, administrator = ? WHERE korisnickoIme = ?";
			jdbcTemplate.update(sql, korisnik.getLozinka(), korisnik.getEmail(), korisnik.getUloga(), korisnik.isAdministrator(), korisnik.getKorisnickoIme());
		}
	}
	@Override
	public void delete(String korisnickoIme) {
		String sql = "DELETE FROM korisnici WHERE korisnickoIme = ?";
		jdbcTemplate.update(sql, korisnickoIme);
	}
}
