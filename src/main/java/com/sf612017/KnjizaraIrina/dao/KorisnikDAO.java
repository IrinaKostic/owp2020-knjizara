package com.sf612017.KnjizaraIrina.dao;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

import com.sf612017.KnjizaraIrina.Model.Korisnik;



public interface KorisnikDAO {
	
	public Korisnik findOne(String korisnickoIme);

	public Korisnik findOne(String korisnickoIme, String lozinka);

	public List<Korisnik> findAll();

	public List<Korisnik> find(String korisnickoIme,String lozinka,String email,String ime,
			String prezime,LocalDate datumRodjenja,String adresa,int brTelefona,LocalDateTime datumVremeReg,String uloga, Boolean administrator);
	
	public void save(Korisnik korisnik);

	public void update(Korisnik korisnik);

	public void delete(String korisnickoIme);

}
