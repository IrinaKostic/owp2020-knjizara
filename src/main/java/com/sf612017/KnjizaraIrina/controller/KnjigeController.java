package com.sf612017.KnjizaraIrina.controller;

import java.io.IOException;
import java.time.LocalDate;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.context.ServletContextAware;
import org.springframework.web.servlet.ModelAndView;

import com.sf612017.KnjizaraIrina.Model.Knjiga;
import com.sf612017.KnjizaraIrina.Model.Korisnik;
import com.sf612017.KnjizaraIrina.service.KnjigaService;



@Controller
@RequestMapping(value="/Knjige")
public class KnjigeController implements ServletContextAware {

	
	@Autowired
	private KnjigaService knjigaService;

	
	@Autowired
	private ServletContext servletContext;
	private String baseURL;

	@Override
	public void setServletContext(ServletContext servletContext) {
		this.servletContext = servletContext;
	} 

	@PostConstruct
	public void init() {
		baseURL = servletContext.getContextPath() + "/";
	}

	@GetMapping
	public ModelAndView index(
			
			@RequestParam(required=false) Long Id,
			@RequestParam(required=false) String naziv, 
			@RequestParam(required=false) Integer ISBN,
			@RequestParam(required=false) String izdavackaKuca, 
			@RequestParam(required=false) String autor, 
			@RequestParam(required=false) LocalDate godIzdavanja,
			@RequestParam(required=false) String opis,
			@RequestParam(required=false) Double cena, 
			@RequestParam(required=false) Integer brStranica,
			@RequestParam(required=false) String tipPoveza,
			@RequestParam(required=false) String pismo,
			@RequestParam(required=false) String jezik,
			@RequestParam(required=false) Integer prosecnaOcena, 
			
			
			HttpSession session)  throws IOException {
		
		if(naziv!=null && naziv.trim().equals(""))
			naziv=null;
		
		// čitanje
		List<Knjiga> knjige = knjigaService.find(Id,naziv, ISBN,izdavackaKuca, autor, godIzdavanja,opis, cena, brStranica,tipPoveza,pismo,jezik,prosecnaOcena);
		

		// prosleđivanje
		ModelAndView rezultat = new ModelAndView("knjige");
		rezultat.addObject("knjige", knjige);
		

		return rezultat;
	}

	@GetMapping(value="/Details")
	@SuppressWarnings("unchecked")
	public ModelAndView details(@RequestParam Long id, 
			HttpSession session, HttpServletRequest request, HttpServletResponse response) throws IOException {
		// čitanje
		Knjiga knjiga = knjigaService.findOne(id);
	


		// prosleđivanje
		ModelAndView rezultat = new ModelAndView("knjiga");
		rezultat.addObject("knjiga", knjiga);
		

		return rezultat;
	}

	@GetMapping(value="/Create")
	public ModelAndView create(HttpSession session, HttpServletResponse response) throws IOException {
		// autentikacija, autorizacija
		Korisnik prijavljeniKorisnik = (Korisnik) session.getAttribute(KorisnikController.KORISNIK_KEY);
		if (prijavljeniKorisnik == null || !prijavljeniKorisnik.isAdministrator()) {
			response.sendRedirect(baseURL + "Knjige");
			return null;
		}

		ModelAndView rezultat = new ModelAndView("dodavanjeKnjige");
		return rezultat;
	}

	@PostMapping(value="/Create")
	public void create(@RequestParam Long Id,@RequestParam String naziv,@RequestParam Integer ISBN,@RequestParam String izdavackaKuca,
			@RequestParam String autor,@RequestParam LocalDate godIzdavanja,@RequestParam String opis,
			@RequestParam Double cena,@RequestParam Integer brStranica,@RequestParam String tipPoveza,
			@RequestParam String pismo,@RequestParam String jezik,@RequestParam Integer prosecnaOcena,
			
			HttpSession session, HttpServletResponse response) throws IOException {
		// autentikacija, autorizacija
		Korisnik prijavljeniKorisnik = (Korisnik) session.getAttribute(KorisnikController.KORISNIK_KEY);
		if (prijavljeniKorisnik == null || !prijavljeniKorisnik.isAdministrator()) {
			response.sendRedirect(baseURL + "Knjige");
			return;
		}

		// kreiranje
		Knjiga knjiga = new Knjiga(Id,naziv, ISBN,izdavackaKuca,autor, godIzdavanja, opis, cena, brStranica,tipPoveza,pismo,jezik,prosecnaOcena);
		knjigaService.save(knjiga);

		response.sendRedirect(baseURL + "Knjige");
	}

	@PostMapping(value="/Edit")
	public void edit(@RequestParam Long Id,@RequestParam String naziv,@RequestParam Integer ISBN,@RequestParam String izdavackaKuca,
	@RequestParam String autor,@RequestParam LocalDate godIzdavanja,@RequestParam String opis,
	@RequestParam Double cena,@RequestParam Integer brStranica,@RequestParam String tipPoveza,
	@RequestParam String pismo,@RequestParam String jezik,@RequestParam Integer prosecnaOcena, 
			HttpSession session, HttpServletResponse response) throws IOException {
		// autentikacija, autorizacija
		Korisnik prijavljeniKorisnik = (Korisnik) session.getAttribute(KorisnikController.KORISNIK_KEY);
		if (prijavljeniKorisnik == null || !prijavljeniKorisnik.isAdministrator()) {
			response.sendRedirect(baseURL + "Knjige");
			return;
		}

		// validacija
		Knjiga knjiga = knjigaService.findOne(Id);
		if (knjiga == null) {
			response.sendRedirect(baseURL + "Knjige");
			return;
		}	
	

		// izmena
		knjiga.setId(Id);
		knjiga.setNaziv(naziv);
		knjiga.setISBN(ISBN);
		knjiga.setIzdavackaKuca(izdavackaKuca);
		knjiga.setAutor(autor);
		knjiga.setGodinaIzdavanja(godIzdavanja);
		knjiga.setOpis(opis);
		knjiga.setCena(cena);
		knjiga.setBrStranica(brStranica);
		knjiga.setTipPoveza(tipPoveza);
		knjiga.setPismo(pismo);
		knjiga.setJezik(jezik);
		knjiga.setProsecnaOcena(prosecnaOcena);
		
		knjigaService.update(knjiga);

		response.sendRedirect(baseURL + "Knjige");
	}

	@PostMapping(value="/Delete")
	public void delete(@RequestParam Long id, 
			HttpSession session, HttpServletResponse response) throws IOException {
		// autentikacija, autorizacija
		Korisnik prijavljeniKorisnik = (Korisnik) session.getAttribute(KorisnikController.KORISNIK_KEY);
		if (prijavljeniKorisnik == null || !prijavljeniKorisnik.isAdministrator()) {
			response.sendRedirect(baseURL + "Knjige");
			return;
		}

		// brisanje
		knjigaService.delete(id);
	
		response.sendRedirect(baseURL + "Knjige");
	}

}

