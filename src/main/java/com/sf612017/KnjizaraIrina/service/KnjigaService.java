package com.sf612017.KnjizaraIrina.service;

import java.time.LocalDate;
import java.util.List;

import com.sf612017.KnjizaraIrina.Model.Knjiga;



public interface KnjigaService {
	
	Knjiga findOne(Long id);
	List<Knjiga> findAll();
	Knjiga save(Knjiga knjiga);
	List<Knjiga> save(List<Knjiga> knjige);
	Knjiga update(Knjiga knjiga);
	List<Knjiga> update(List<Knjiga> knjige);
	Knjiga delete(Long id);
	void delete(List<Long> ids);
	List<Knjiga> find(Long id, String naziv, int iSBN, String izdavackaKuca, String autor, LocalDate godinaIzdavanja,
			String opis, Double cena, int brStranica, String tipPoveza, String pismo, String jezik, int prosecnaOcena);
	

}
